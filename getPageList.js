var fs = require("fs");
var scraper = require('./modules/scraper');
var _ = require('lodash');

var bggUrl = 'http://boardgamegeek.com/geeklist/197039';
var bggPagination = {
  scope: 'div.pager',
  selector: 'a[title*="last"]',
  postProcessor: function (results) {
    var lastPageMatcher = /(\d+)/;

    return lastPageMatcher.exec(results)[1];
  }
};

var bgg = {
  scope: 'div.mb5',
  selector: [{
    number: 'div.fl a:nth-child(1)',
    link: 'div.fl a:nth-child(1)@href',
    name: 'div.fl a:nth-child(2)',
    ranking: 'div.fl a:last-child',
    description: 'dd.doubleright@html'
  }]
};

// We need to know a list of pages we should process. That list is the last
// page we saw last time, plus any pages past it.
scraper.scrapeSite(bggUrl, bggPagination).then(function (lastPage) {
  lastPage = parseInt(lastPage, 10);
  var urls = [];

  // Generate a set of URLs to scrape.
  _.each(_.range(1, lastPage + 1), function (pageNumber) {
    urls.push({
      pageNumber: pageNumber,
      url: `https://boardgamegeek.com/geeklist/197039/virtual-flea-market-delivery-bggcon-2015/page/${pageNumber}`
    });
  });

  process.stdout.write(JSON.stringify(urls));
});
