var Q = require('q');
var Xray = require('x-ray'); // https://github.com/lapwinglabs/x-ray
var x = Xray();

// This uses the x-ray library to scrape a single site. It does a _fantastic_
// job of it and all you have to do is give it a simple schema describing what
// you want pulled and into which values it should be put.
function scrapeSite(url, site) {
  var scraped = Q.defer();

  x(url, site.scope, site.selector)(function (err, results) {
      if (site.postProcessor) {
        scraped.resolve(site.postProcessor(results));
      } else {
        scraped.resolve(results);
      }
    });

  return scraped.promise;
}

module.exports = {
  scrapeSite: scrapeSite
};
