var http = require('http');
var url = require('url');

// Send a notification to IFTTT and then that can, in turn, take whatever
// action.
var iftttNotificationUrl =
  'https://maker.ifttt.com/trigger/gameListing/with/key/brBN4gGt9VUYMDu2Tzv3oU';

function notifyIFTTT(json) {
  var parsedUrl = url.parse(iftttNotificationUrl);
  var postData = JSON.stringify(json);

  // An object of options to indicate where to post to
  var post_options = {
      hostname: parsedUrl.hostname,
      port: parsedUrl.port,
      path: parsedUrl.path,
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Content-Length': postData.length
      }
  };

  // Set up the request
  var post_req = http.request(post_options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
      });
  });

  // post the data
  post_req.write(postData);
  post_req.end();
}

module.exports = {
  notify: notifyIFTTT
};
