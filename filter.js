var fs = require("fs");
var ifttt = require('./modules/ifttt');
var _ = require('lodash');
if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

// Load all the items and send each one to IFTTT.
var stdin = process.stdin;
var stdout = process.stdout;

var inputChunks = [];

stdin.resume();
stdin.setEncoding('utf8');

stdin.on('data', function (chunk) {
  inputChunks.push(chunk);
});

stdin.on('end', function () {
  var items = JSON.parse(inputChunks.join(''));

  // Find the previous highest item seen.
  var maxItemNumber = _.parseInt(localStorage.getItem('maxItemNumber'));
  var newMaxItemNumber = maxItemNumber;
  var filteredItems = [];

  _.each(items.items, function (item) {
    // If it's new, we keep it. If not, we drop it on the floor.
    if (item.number > maxItemNumber) {
      filteredItems.push(item);
    }

    if (item.number > newMaxItemNumber) {
      newMaxItemNumber = item.number;
    }
  });

  // Record the maximum number seen on an item.
  localStorage.setItem('maxItemNumber', newMaxItemNumber);

  stdout.write(JSON.stringify({
    lastPage: items.lastPage,
    lastItem: items.lastItem,
    items: filteredItems
  }));
});
