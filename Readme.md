I wrote this as a series of filters with each one having an output and many
taking an input. It made it much easier to write each one independently and
add new steps over time. Also it made it easy to use the tool "jq" to perform
manipulation of JSON at several points without writing a new program to do that
step.

Obviously this could get a lot better if it automatically filtered games I already own or previously owned, could do something special if it is something on my wishlist, or even took feedback from me so I could tell it to skip a particular title if someone else lists a copy but I'm not interested. But it's good enough to be useful right now even without all that stuff.

## Installation

1. You're going to need Node.js if you don't already have it
([https://nodejs.org/en/](https://nodejs.org/en/)). The installation link is right there on the home page.
I used version 4.2.2 LTS for my work but any version 4 should work.

1. Download everything in this Git repository. If you're familiar with Git, that
should be easy, but if not, don't panic. Instead just click the icon to the
left which looks like a cloud with an arrow coming down out of it. That will
let you "Download repository" as a ZIP file. Unzip it in a directory and go
there via your command line.

1. Run "npm install" to install all the Node.js packages I used when building this.

1. Download and install jq ([https://stedolan.github.io/jq/](https://stedolan.github.io/jq/)). You could definitely
make a pipeline of stuff without it but I used it so I could get by with fewer
programs to manipulate the JSON output of the various apps.

## Starting/Running

node quasiJohn.js

## Explanation

There are four programs here and one program which runs them:

**quasiJohn.js** - This runs all of the other programs and jq as needed to do all
the scraping, filtering, etc. It also does the scheduling. I could have setup
something with a shell script and cron but by doing it this way it works
cross-platform and I could inject some JavaScript in there if anything got
complicated. Plus, I don't know much shell scripting.

**getPageList.js** - Goes to the BGG.con 2015 VFM geeklist and finds the highest
page number there. It then generates a list of URLs for every page in the VFM.

[At this point jq is used to filter that list to remove X number of early pages
because they shouldn't need to be scraped again]

**getItemsFromPages.js** - Receives a list of pages to scrape and scrapes each one
in turn for items. Those items are then turned into a single JSON object which
list everything found along with some other info later apps need like the last
page scraped and last item found.

[jq gets used again here to remove everything unranked (stuff like expansions
and promos) and everything with a BGG rank below 1000 from the list]

**filter.js** - This is a simple app which keeps track of the geeklist item number
of the last item we've seen so far. If the scraped item has a number below that
we've already sent it on and we don't want repeats about the same stuff. So it
filters it out. If it's new it passes it on into the next list and increases
the number of the highest item passed on. This can end up with items being
skipped when people remove stuff from the list but it was simple and didn't
require me to setup a database of seen vs. unseen.

**notifyIFTTT.js** - This builds a set of information to pass to "If This Then That"
(IFTTT). They added the ability for you to add a channel where you can trigger
events by calling a URL and posting some information. The code to actually
perform the call is in the modules/ifttt.js file. **Note: If you run this without
changing the key in the URL then I will get the notifications, not you. Please
don't do that.** Instead, add the "Maker Channel" on your IFTTT account and create
a new event and then put in your own URL into the ifttt.js file. At that point
you can hook anything you like up on the "Then That" portion of the equation.

I mainly used GMail, SMS, or Philips Hue but you can go nuts with stuff.
